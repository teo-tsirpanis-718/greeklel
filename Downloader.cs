﻿using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Greeklel
{
    internal static class Downloader
    {
        private const string JokesServer = "http://paredoraki.gr/admin/fetch-kammena.php?";

        public static async Task<string[]> DownloadJokes(bool recent)
        {
            string jokesxml;
            using (var http = new HttpClient())
            {
                http.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/4.0 (compatible; Greeklel)");
                jokesxml =
                    Encoding.UTF8.GetString(
                        await http.GetByteArrayAsync(JokesServer + (recent ? "today=1" : "count=90")));
            }
            return
                XDocument.Parse(jokesxml)
                    .Element("news")?.Descendants()
                    .Select(node => node.Attribute("desc")?.Value)
                    .Distinct()
                    .ToArray();
        }
    }
}