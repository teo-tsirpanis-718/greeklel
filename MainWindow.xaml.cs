﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Greeklel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async Task ShowJokes(bool recent)
        {
            try
            {
                Button.IsEnabled = false;
                Button1.IsEnabled = false;
                var jokes = await Downloader.DownloadJokes(recent);
                HtmlPanel.Text = $"<ul> {string.Join("<br>", jokes.Select(j => $"<li> {j} </li>"))} </ul>";
            }
            finally
            {
                Button.IsEnabled = true;
                Button1.IsEnabled = true;
            }
        }

        private async void Button_OnClick(object sender, RoutedEventArgs e) => await ShowJokes(true);

        private async void Button1_OnClick(object sender, RoutedEventArgs e) => await ShowJokes(false);

        private void UIElement_OnMouseUp(object sender, MouseButtonEventArgs e)
            => Process.Start("https://iconsmind.com");

        private void UIElement_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
            => Process.Start("https://gitlab.com/teo-tsirpanis-718/greeklel");
    }
}